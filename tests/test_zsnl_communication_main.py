# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from unittest import mock
from zsnl_communication_consumer import __main__ as main


class TestMain:
    """Tests for the consumer classes"""

    @mock.patch("zsnl_communication_consumer.__main__.InfrastructureFactory")
    @mock.patch("zsnl_communication_consumer.__main__.CQRS")
    @mock.patch("zsnl_communication_consumer.__main__.communication")
    @mock.patch(
        "zsnl_communication_consumer.__main__.DatabaseTransactionMiddleware"
    )
    @mock.patch("zsnl_communication_consumer.__main__.AmqpPublisherMiddleware")
    @mock.patch("zsnl_communication_consumer.__main__.AMQPClient")
    def test_main(
        self,
        mock_amqp,
        mock_amqp_publisher,
        mock_database_transaction,
        mock_communication,
        mock_cqrs,
        mock_infra_factory,
    ):
        main.main()

        mock_infra_factory.assert_called_once_with(config_file="config.conf")

    def test_log_record_factory(self):
        record = main.log_record_factory(
            name="foo",
            level="WARN",
            pathname="/dev/null",
            lineno=42,
            args={},
            msg="boo",
            exc_info=None,
        )

        assert record.msg == "boo"
        assert record.zs_component == "zsnl_communication_consumer"
